package main

import "os"
import "./server"
import "./instance"

func main() {
	if (len(os.Args) < 2) {
		panic("no action");
	}
	var action string = os.Args[1];
	switch(action){
	case "run":
		if (len(os.Args) < 3) {
			panic("missed configuration file param");
		}
		if (len(os.Args) < 4) {
			panic("missed run type param");
		}
		switch os.Args[2] {
		case "server":
			server.Run(os.Args[3])
		case "instance":
			instance.Run(os.Args[3])
		default:
			panic("can run 'server' or 'instance' only");
		}

	default:
		panic("illegal action")
	}
}
