package instance

import "fmt"
import "os"
import "github.com/BurntSushi/toml"

func Run(filePath string) {
	fmt.Println("parsing configuration file " + filePath)
	config := readConfig(filePath)
	fmt.Printf("Running server '%s' with storage size '%sMb' in directory '%s'\n", config.Server.Name, config.Server.Sizemb, config.Server.Path)
	fmt.Printf("Instance type: %s\n", config.Server.InstanceType);
}

type Config struct {
	Server struct {
		       InstanceType string
		       Name         string
		       Sizemb       string
		       Path         string
	       }
}

func readConfig(filePath string) Config {
	_, err := os.Stat(filePath)
	if err != nil {
		panic("Config file is missing: " + filePath)
	}

	var config Config
	if _, err := toml.DecodeFile(filePath, &config); err != nil {
		panic(err)
	}
	return config
}