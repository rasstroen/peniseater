package server

import "../instance"

type ServerConfig struct {
	Server   struct {
			 Host string
			 Port int
		 }
	Instance []instance.InstanceConfig
}