package instance

type InstanceConfig struct {
	Id   int
	Host string
	Port int
}

const STATUS_UNKNOWN = 0;