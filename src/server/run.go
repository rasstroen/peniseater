package server

import "fmt"
import "os"
import "github.com/BurntSushi/toml"
import "./http"
import "../lib/server"



func Run(filePath string) {
	fmt.Println("parsing configuration file " + filePath)
	config := readConfig(filePath)
	fmt.Printf("\nRunning server '%s:%d' \n", config.Server.Host, config.Server.Port)
	http.Work(config);
}

func readConfig(filePath string) server.ServerConfig {
	_, err := os.Stat(filePath)
	if err != nil {
		panic("Config file is missing: " + filePath)
	}

	var config server.ServerConfig
	if _, err := toml.DecodeFile(filePath, &config); err != nil {
		panic(err)
	}
	return config
}