package http

import "fmt"
import "../../lib/instance"
import "../../lib/server"
import "net/http"
import "strconv"
import "time"

var Instances InstanceStatuses

type InstanceStatuses[] struct {
	Config     instance.InstanceConfig
	StatusCode int
}

func Work(serverConfiguration server.ServerConfig) {
	fmt.Println("Master process starting")

	Instances = make(InstanceStatuses, len(serverConfiguration.Instance));

	for instanceIndex, instanceConfiguration := range serverConfiguration.Instance {
		initInstance(instanceIndex, instanceConfiguration)
	}
	debugInstanceStatuses()
	go backgroundWork()
	http.HandleFunc("/", handler)
	http.ListenAndServe(":" + strconv.Itoa(serverConfiguration.Server.Port), nil)
}

func backgroundWork() {
//	for {}
}

func handler(response http.ResponseWriter, request *http.Request) {
	var command string = request.URL.Path[1:]
	switch command {
	case "status":
		fmt.Fprintf(response, debugInstanceStatuses())
	default:
		fmt.Fprintf(response, debugInstanceStatuses())
	}
}

func initInstance(instanceIndex int, instanceConfiguration instance.InstanceConfig) {
	Instances[instanceIndex].Config = instanceConfiguration;
	Instances[instanceIndex].StatusCode = instance.STATUS_UNKNOWN;
}

func debugInstanceStatuses() string {
	t := time.Now()
	var statusString string = t.Format(time.UnixDate) + "\n";
	for instanceIndex, Instance := range Instances {
		statusString = statusString + fmt.Sprintf("\tInstance id:%d '%v' \n", instanceIndex, Instance)
	}
	return statusString
}
